#ifndef AOC2023_KIKI_HPP
#define AOC2023_KIKI_HPP

#include <string>
#include <vector>
#include <sstream>
#include <ranges>

int extractNumber(const std::string& str, int startInd, int currInd = -1, int endInd = -1,  int* nextInd = nullptr) {
    if(startInd == -1 && currInd == -1 && endInd == -1) {
        throw std::logic_error("");
    }
    
    std::stringstream ss;
    int k;  // TmpIndex
    int convNum, n = str.size();

    if(startInd == -1) {
        // Find the start from currInd or endInd
        k = (currInd != -1) ? (currInd-1) : (endInd-1);

        while(k >= 0 && isdigit(str[k])) {
            --k;
        }

        startInd = k+1;
    }

    if(endInd == -1) {
        k = (currInd != -1) ? (currInd+1) : (startInd+1);

        while(k < n && isdigit(str[k])) {
            ++k;
        }

        endInd = k-1;

        convNum = std::stoi(str.substr(endInd - startInd + 1));
        
        if(nextInd != nullptr) {
            *nextInd = endInd + 1;
        }

        return convNum;
    }
}

template<typename T>
concept Container = requires(T container) {
    typename T::value_type;
    { std::begin(container) } -> std::same_as<typename T::iterator>;
    { std::end(container) } -> std::same_as<typename T::iterator>;
    requires std::ranges::range<T>;
};

template<typename T>
concept StringContainer = Container<T>  && std::same_as<typename T::value_type, std::string>;

template <StringContainer C, Container R>
R stringNums2IntNums(const C& container) {
    R toRet;

    bool emplaceBackCont = std::is_invocable_v<decltype(&R::emplace_back), R>;

    int tmpNum;
    for (const std::string& element : container) {
        // Convert each string element to an integer
        tmpNum = std::stoi(element);

        if(emplaceBackCont) {
            toRet.emplace_back(tmpNum);
        } else {
            toRet.emplace(tmpNum);
        }
    }

  return toRet;
}


template<typename T>
T& split(const std::string& input, T& container, char delim = '\n') {
    std::istringstream iss{input};

    bool emplaceBackCont = std::is_invocable_v<decltype(&T::emplace_back), T, std::string&&>>;

    string token;
    while(std::getline(iss, token, delimiter)) {
        if (emplaceBackCont) {
            container.emplace_back(std::move(token));
        } else {
            container.emplace(std::move(token));
        }
    }

    return container;
}

#endif