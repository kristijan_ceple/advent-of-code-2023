#include <iostream>
#include <sstream>
#include <vector>
#include <unordered_set>
#include <unordered_map>
using namespace std;

struct PairHash {
    template<class T1, class T2>
    size_t operator()(const pair<T1, T2>& p) const {
        auto h1 = std::hash<T1>{}(p.first);
        auto h2 = std::hash<T2>{}(p.second);

        return h1^h2;
    }
};

enum class Compass { N, S, W, E, NW, NE, SE, SW };

class Solution {
private:
    static constexpr char FILLER_CH = '.';
    static constexpr char GEAR_CH = '*';
    static constexpr int TARGET_SQUARE_SUM = 8;

    const vector<string>& data;
    const int rows, cols;

    unordered_set<int> processedNumInds;
    unordered_set<pair<int, int>, PairHash> partsIndices;

    int64_t solSum = 0;

    pair<int, pair<int, int>> processNumber(int i, int j, int tmpStart = -1, int tmpEnd = -1, bool forceProcessing = false) {
        int tmpNum, flatIndex;
        int k;
        
        if(tmpStart == -1) {
            // Find start of num first
            k = j-1;
            while(k >= 0 && isdigit(data[i][k])) {
                --k;
            }
            tmpStart = k + 1;
        }
        
        flatIndex = i * rows + tmpStart;
        if(processedNumInds.contains(flatIndex) && !forceProcessing) {
            return make_pair(-1, make_pair(-1, -1));
        }

        if(tmpEnd == -1) {
            k = j+1;
            while(k < cols && isdigit(data[i][k])) {
                ++k;
            }
            tmpEnd = k - 1;
        }
        
        tmpNum = stoi(data[i].substr(tmpStart, tmpEnd-tmpStart+1));
        // solSum += tmpNum;
        processedNumInds.emplace(flatIndex);

        for(k = tmpStart; k <= tmpEnd; ++k) {
            partsIndices.emplace(make_pair(i, k));
        }

        return make_pair(tmpNum, make_pair(tmpStart, tmpEnd));
    }

    void processSpecialSymbol(int i, int j) {
        int sum = 0;

        int tmpStart, tmpEnd, tmpNum, flatIndex;
        int k;

        // Let's check left first
        if(j > 0 && isdigit(data[i][j-1])) {
            processNumber(i, j-1, -1, j-1);
        }

        // Let's check right!
        if(j < cols-1 && isdigit(data[i][j+1])) {
            processNumber(i, j+1, j+1, -1);
        }

        // Time to check up
        if(i > 0 && isdigit(data[i-1][j])) {
            processNumber(i-1, j);
        }

        // Check down
        if(i < rows-1 && isdigit(data[i+1][j])) {
            processNumber(i+1, j);
        }

        // NW
        if(i > 0 && j > 0 && isdigit(data[i-1][j-1])) {
            processNumber(i-1, j-1);
        }

        // NE
        if(i > 0 && j < cols-1 && isdigit(data[i-1][j+1])) {
            processNumber(i-1, j+1);
        }

        // SE
        if(i < rows-1 && j < cols-1 && isdigit(data[i+1][j+1])) {
            processNumber(i+1, j+1);
        }

        // SW
        if(i < rows-1 && j > 0 && isdigit(data[i+1][j-1])) {
            processNumber(i+1, j-1);
        }
    }   


    void processGear(int i, int j) {
        // CHECK THE CORNAHS!
        vector<pair<int, int>> compasses;

        // N - up - OK
        if(i > 0 && isdigit(data[i-1][j])) {
            compasses.emplace_back(make_pair(i-1, j));
        }

        // S - down - OK
        if(i < rows-1 && isdigit(data[i+1][j])) {
            compasses.emplace_back(make_pair(i+1, j));
        }

        // W - left - OK
        if(j > 0 && isdigit(data[i][j-1])) {
            compasses.emplace_back(make_pair(i, j-1));
        }

        // E - right - OK
        if(j < cols-1 && isdigit(data[i][j+1])) {
            compasses.emplace_back(make_pair(i, j+1));
        }

        // NE - up & right - OK
        if(i > 0 && j < cols-1 && isdigit(data[i-1][j+1])) {
            compasses.emplace_back(make_pair(i-1, j+1));
        }

        // SE - down & right - OK
        if(i < rows-1 && j < cols-1 && isdigit(data[i+1][j+1])) {
            compasses.emplace_back(make_pair(i+1, j+1));
        }

        // SW - down & left - OK
        if(i < rows-1 && j > 0 && isdigit(data[i+1][j-1])) {
            compasses.emplace_back(make_pair(i+1, j-1));
        }

        // NW - up & left - OK
        if(i > 0 && j > 0 && isdigit(data[i-1][j-1])) {
            compasses.emplace_back(make_pair(i-1, j-1));
        }

        unordered_map<pair<int, int>, int, PairHash> gearParts;     // [start, end] -> num value
        for(const auto& currPair : compasses) {
            auto tmp = this->processNumber(currPair.first, currPair.second, -1, -1, true);
            gearParts[tmp.second] = tmp.first;
        }

        if(gearParts.size() == 2) {
            int64_t product = 1;
            for(const auto& currPair : gearParts) {
                product *= static_cast<int64_t>(currPair.second);
            }

            solSum += product;
        }
    }
public:
    Solution(const vector<string>& data) : data{data}, rows{data.size()}, cols{data[0].size()} {};

    int solveTask() {
        /*
        1. Find the special symbols
        2. Check the square around the special symbol
            2a) Init checkSum to 0
            2b) If dot increase checkSum
            2c) If not dot, form whole number, make sure to increase checkSum if located in immediate neighbouring square
            2d) Stop when checkSum = 8 (whole square checked)
        3. Sum all the numbers around these special symbols TA-DA!

        Extra notes: Some numbers may be present for 2 or more rectangles
        Store indices in a set therefore
        Set can be periodically refreshed if necessary but I dont think that will be necessary
        */
        char currChar;
        // for(int i = 0; i < rows; ++i) {
        //     for(int j = 0; j < cols; ++j) {
        //         currChar = data[i][j];
        //         if(currChar == FILLER_CH || isdigit(currChar)) {
        //             continue;
        //         }

        //         // Else we have a special symbol
        //         this->processSpecialSymbol(i, j);
        //     }
        // }

        // Okay, after finding out all part number indices it's time to handle the gears
        for(int i = 0; i < rows; ++i) {
            for(int j = 0; j < cols; ++j) {
                currChar = data[i][j];
                if(currChar == GEAR_CH) {
                    this->processGear(i, j);
                }
            }
        }


        return this->solSum;
    }

};

int main(void) {
    string input = R"(467..114..
...*......
..35..633.
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598..)";

    // First process the string into memory
    vector<string> data;
    istringstream iss(input);

    string line;
    while(getline(iss, line, '\n')) {
        data.emplace_back(move(line));
    }

    Solution sol(data);
    int res = sol.solveTask();
    cout << "Res: " << res << endl;


    return 0;
}