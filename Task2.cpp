#include <string>
#include <iostream>
#include <array>
#include <vector>
using namespace std;

class Solution {
private:
    enum class Colour {Red = 0, Green = 1, Blue = 2};
    inline static const array<string, 3> COLOURS_NAMES = {"red", "green", "blue"};
    static constexpr array<int, 3> COLOURS_LIMITS = {12, 13, 14};

    int currStringIndex = 0;
    const string& inputString;
    const int inputSize;

    int solSum = 0;
public:
    Solution(const string& inputString) : inputString{inputString}, inputSize{inputString.size()} {};

    inline int extractNumber() {
        int numBegin = -1;
        int numEnd = -1;
        char currChar;

        while(true) {
            currChar = inputString[currStringIndex];
            
            if(numBegin == -1 && isdigit(currChar)) {
                numBegin = currStringIndex;
            } else if(numEnd == -1 && !isdigit(currChar)) {
                numEnd = currStringIndex-1;
                break;
            }

            ++currStringIndex;
        }

        return std::stoi(inputString.substr(numBegin, numEnd-numBegin+1));
    }

    inline Colour extractColour() {
        char currCh = inputString[currStringIndex];
        Colour toRet;
        switch(currCh) {
            case 'r':
                toRet = Colour::Red;
                break;
            case 'g':
                toRet = Colour::Green;
                break;
            case 'b':
                toRet = Colour::Blue;
                break;
            default:
                throw logic_error("Invalid starting letter!");
        }

        currStringIndex += COLOURS_NAMES[static_cast<int>(toRet)].size();
        return toRet;
    }

    bool processSets() {
        while(inputString[currStringIndex] != '\n' && inputString[currStringIndex] != '\0') {
            while(!isdigit(inputString[currStringIndex])) {
                ++currStringIndex;
            }

            bool res = this->processSet();
            if(!res) {
                return false;
            }
        }

        return true;
    }

    bool processSet() {
        // Go until we reach ';' or '\n'
        int tmpNum;
        Colour tmpCol;
        while(inputString[currStringIndex] != '\n' && inputString[currStringIndex] != ';' && inputString[currStringIndex] != '\0') {
            // First extract the number
            tmpNum = this->extractNumber();

            // Check which colour this is
            ++currStringIndex;
            tmpCol = this->extractColour();

            // Check if it's overboard
            if(tmpNum > COLOURS_LIMITS[static_cast<int>(tmpCol)]) {
                return false;
            }

            // Fast forward to next num
            if(inputString[currStringIndex] == ',') {
                while(!isdigit(inputString[currStringIndex])) {
                    ++currStringIndex;
                }
            } 
        }

        return true;
    }

    void processLine() {
        // 1 Line = 1 Game
        // Extract ID until ':'
        // Then extract games separated by ';' until EOL is reached
        // Check whether each game is possible

        // Skip "Game :" part
        currStringIndex += 5;

        // Okay, get to the number
        int id = this->extractNumber();

        // Check if this game is valid
        currStringIndex += 2;
        bool valid = this->processSets();

        // End processing
        if(valid) {
            solSum += id;
        } else {
            // Skip until the next 'G'
            while(inputString[currStringIndex] != '\n' && inputString[currStringIndex] != '\0') {
                ++currStringIndex;
            }
        }

        // Skip to next line
        ++currStringIndex;
    }


    int solveTask() {
        // Start iteration and detect lines
        this->currStringIndex = 0;
        while(this->currStringIndex < this->inputSize) {
            this->processLine();
        }

        return this->solSum;
    }
};

int main(void) {
    string input = R"(Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green
)";
    Solution sol(input);

    int res = sol.solveTask();
    cout << "Res: " << res << endl;

    return 0;
}